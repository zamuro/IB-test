class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.string :keywords
      t.string :categories

      t.timestamps null: false
    end
  end
end
