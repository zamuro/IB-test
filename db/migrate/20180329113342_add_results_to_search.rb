class AddResultsToSearch < ActiveRecord::Migration
  def change
    add_column :searches, :result, :text
  end
end
