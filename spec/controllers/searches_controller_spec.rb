require 'rails_helper'

RSpec.describe SearchesController, type: :controller do

  describe "GET #new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "returns http success" do
      body = [
        {
          category:  "null",
          icon_url:  "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
          id:  "dCPAZRWsQ5eUw8SuMQuacg",
          url: "https://api.chucknorris.io/jokes/dCPAZRWsQ5eUw8SuMQuacg",
          value: "Chuck Norris walked past Uasin Bolt who was running the 100m... on the day that he broke the world record",
        }
      ].to_json
      stub_request(:get, "https://api.chucknorris.io/jokes/random").
      to_return(status: 200, body: body)
    end
  end

end
