require 'rails_helper'

RSpec.describe Search, type: :model do
  before do
    @search = Search.new(keywords: "roundhouse kick", categories: "undefined", result: "It used to be called goodminton before Chuck Norris tried it.")
  end

  context "in search" do
    it "create new search by keywords" do
      expect(@search.keywords).to eq("roundhouse kick")
    end

    it "create new search by categories" do
      expect(@search.categories).to eq("undefined")
    end

    it "gets a result" do
      expect(@search.result).to include("goodminton")
    end
  end
end
